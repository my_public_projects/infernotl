#ifndef ITL_RED_BLACK_TREE_H
#define ITL_RED_BLACK_TREE_H
#include "itl_defines.h"
#include "itl_list.h"
namespace itl
{
template <typename Key, typename Value>
/*!
 * \brief The red black tree template class. Why? Needs for realization the map container
 * \author InfernalBoy
 * \details This class is not own any data. It used only for find.
 */
class rb_tree
{
    using rbt_data = typename list<pair<Key, Value>>::list_iterrator;

    /*!
     * \brief The rbt_node struct
     * \details node of the red black tree
     */
    struct rbt_node
    {
       rbt_node * m_left = nullptr;
       rbt_node * m_right = nullptr;
       rbt_node * m_parent = nullptr;
       bool m_red = true;
       rbt_data m_data;


       /*!
        * \brief rbt_node(const rbt_data &, rbt_node * parent=nullptr)
        * \param data [in]
        * \param parent [in]
        * \details Constructs new rbt node with initialization of data and parent node
        */
       rbt_node(const rbt_data & data,
                rbt_node * parent=nullptr)
           : m_parent(parent),
             m_data(data)

       {}


       ~rbt_node()
       {
           m_left = nullptr;
           m_right = nullptr;
           m_parent = nullptr;

       }


       //TODO to non recursive method of rb_tree class
       /*!
        * \brief destroy() - recursive estroying cildren nodes
        */
       void destroy()
       {
           if(m_left)
           {
               m_left->destroy();
               delete m_left;
               m_left = nullptr;
           }
           if(m_right)
           {
               m_right->destroy();
               delete m_right;
               m_right = nullptr;
           }

       }


       /*!
        * \brief swap_child(rbt_node *, rbt_node *)
        * \param old [in]
        * \param _new [in]
        * \details swaps pointer to the children node from old to _new
        */
       inline void swap_child(rbt_node * old, rbt_node * _new)
       {
           if(m_left == old)
           {
               m_left = _new;
           }
           else
           {
               m_right = _new;
           }

       }


       //TODO to non recursive method of rb_tree class
       /*!
        * \brief print() - prints tree's nodes
        *
        */
       void print() const
       {

           std::cout << "l(" << ((m_left) ? m_left->m_data->m_first : 0) << ") me(" << m_data->m_first <<
               ")[" << (m_red ? "red" : "black" ) << ", v:" << m_data->m_second <<  "]  r(" << ((m_right) ? m_right->m_data->m_first : 0) << ")\n";
           if(m_left) m_left->print();
           if(m_right) m_right->print();
       }

    };

    rbt_node * m_root = nullptr;

public:

    /*!
     * \brief rb_tree() - Constructs empty tree
     */
    rb_tree()
    {

    }


    /*!
     * \brief ~rb_tree() - destroys tree and free memory
     *
     */
    ~rb_tree()
    {
        clear();
    }



    /*!
     * \brief insert(const rbt_data & data)
     * \param data [in]
     * \details Creates new node and inserts in RB tree
     */
    void insert(const rbt_data & data)
    {
        insert_node(data);
    }


    /*!
     * \brief find(const Key &)
     * \param key [in]
     * \return returns rbt_data aka list<Key, Value>::list_iterator with founded data.
     * if data with that key is not found returns empty list_iterator
     * \details finds data by key
     */
    rbt_data find(const Key & key)
    {
        rbt_node * res = find_node(key);
        if(!res)
        {
            return rbt_data();
        }
        return res->m_data;
    }


    /*!
     *\brief find(const Key &) const
     * \param key [in]
     * \return  returns rbt_data aka list<Key, Value>::list_iterator with the founded const data.
     * if data with that key is not found returns empty list_iterator
     * \details finds data by key
     */
    const rbt_data find(const Key & key) const
    {
        rbt_node * res = find_node(key);
        if(!res)
        {
            return rbt_data();
        }
        return res->m_data;
    }


    /*!
     * \brief erase(const Key &)
     * \param key [in]
     * \return list_iterator from erased node
     * \details Erase the node by key
     */
    rbt_data erase(const Key & key )
    {
        rbt_node * node = find_node(key);
        rbt_node * to_remove;
        rbt_node * child;
        if(!node)
        {
            return rbt_data(nullptr);
        }

        if(!node->m_left || !node->m_right)
        {
            to_remove = node;
        }
        else
        {
            to_remove = node->m_right;
            while(to_remove->m_left)
            {
                to_remove = to_remove->m_left;
            }
        }


        if(to_remove->m_left)
        {
            child = to_remove->m_left;
        }
        else
        {
            child = to_remove->m_right;
        }

        if(child)
        {
            child->m_parent = to_remove->m_parent;
        }
        else  if(!to_remove->m_red)
        {
            validate_erase(to_remove);
        }


        if(to_remove->m_parent)
        {
            to_remove->m_parent->swap_child(to_remove, child);
            if(child)
            {
                child->m_parent = to_remove->m_parent;
            }
        }
        else if(child)
        {
            to_remove->m_parent = child;
            m_root = child;
        }
        else
        {
            m_root = nullptr;
        }

        if(to_remove != node)
        {
            auto temp = node->m_data;
            node->m_data = to_remove->m_data;
            to_remove->m_data = temp;
        }

        if(!to_remove->m_red && child)
        {
             validate_erase(child);
        }

        auto res = to_remove->m_data;
        delete to_remove;
        return res;
    }

    /*!
     * \brief print() const
     * \details Prints the tree to std::cout
     */
    void print() const
    {
        if(!m_root) {
            std::cout << "\n*****red_black_tree*******\n"
                         "************** is empty***\n";
            return;
        }
        m_root->print();
    }


    /*!
     * \brief clear()
     * \details Clears tree.
     */
    inline void clear()
    {
        m_root->destroy();
        delete m_root;
        m_root = nullptr;
    }

private:

    inline void rot_left(rbt_node * node) noexcept
    {
        assert(node->m_right);
        rbt_node * temp = node->m_right;
        node->m_right = temp->m_left;
        if(node->m_right)
        {
            node->m_right->m_parent = node;
        }
        temp->m_parent = node->m_parent;
        temp->m_left = node;
        if(node->m_parent)
        {
            node->m_parent->swap_child(node, temp);
        }
        else
        {
            m_root = temp;
        }
        node->m_parent = temp;
    }


    inline void rot_right(rbt_node * node) noexcept
    {
        assert(node->m_left);
        rbt_node * temp = node->m_left;
        node->m_left = temp->m_right;
        if(node->m_left)
        {
            node->m_left->m_parent = node;
        }
        temp->m_parent = node->m_parent;
        temp->m_right = node;
        if(node->m_parent)
        {
            node->m_parent->swap_child(node, temp);
        }
        else
        {
            m_root = temp;
        }
        node->m_parent = temp;
    }


    void validate_insert(rbt_node * node) noexcept
    {
        rbt_node * grand_pa;
        rbt_node * uncle;
        rbt_node * parent;
        while(node != m_root && node->m_parent->m_red)
        {
            grand_pa = node->m_parent->m_parent;
            parent = node->m_parent;
           // bool is_left = grand_pa->m_left == parent;
            if(grand_pa->m_left == parent)
            {
                if(parent->m_right == node)
                {
                    rbt_node * temp = parent;
                    rot_left(parent);
                    node = temp;
                    continue;
                }
                else
                {
                    uncle = grand_pa->m_right;
                    if(!uncle)
                    {
                        parent->m_red = false;
                        grand_pa->m_red = true;
                        rot_right(grand_pa);
                        node = parent;
                        continue;
                    }
                    else
                    {
                        if(uncle->m_red)
                        {
                            parent->m_red = false;
                            uncle->m_red = false;
                            grand_pa->m_red = true;
                            node = grand_pa;
                            continue;
                        }
                        else
                        {
                            rot_right(grand_pa);
                            parent->m_red = false;
                            grand_pa->m_red = true;
                            node = parent;
                            continue;
                        }
                    }
                }

            } else
            {
                if(parent->m_left == node)
                {
                    rbt_node * temp = parent;
                    rot_right(temp);
                    node = temp;
                    continue;
                }
                else
                {
                    uncle = grand_pa->m_left;
                    if(!uncle)
                    {
                        parent->m_red = false;
                        grand_pa->m_red = true;
                        rot_left(grand_pa);
                        node = parent;
                        continue;
                    }
                    else
                    {
                        if(uncle->m_red)
                        {
                            parent->m_red = false;
                            uncle->m_red = false;
                            grand_pa->m_red = true;
                            node = grand_pa;
                            continue;
                        }
                        else
                        {
                            rot_left(grand_pa);
                            parent->m_red = false;
                            grand_pa->m_red = true;
                            node = parent;
                            continue;
                        }
                    }

                }
            }
        }
        m_root->m_red = false;
    }


    void validate_erase(rbt_node * node) noexcept
    {
        while(m_root != node && !node->m_red)
        {
            bool is_left = node->m_parent->m_left == node;

            {
                rbt_node * bro =  is_left ? node->m_parent->m_right:
                                            node->m_parent->m_left;

                rbt_node * nephew_a = is_left ? bro->m_right :
                                                bro->m_left;

                rbt_node * nephew_b = !is_left ? bro->m_right :
                                                 bro->m_left;
                if(bro->m_red)
                {
                    bro->m_red = false;
                    node->m_parent->m_red = true;
                    is_left ? rot_left(node->m_parent):
                              rot_right(node->m_parent);
                    bro = node->m_parent->m_right;
                }


                if(node->m_parent->m_red)
                {
                    bro->m_red = true;
                    node->m_parent->m_red = false;
                    break;
                }

                if((!bro->m_left && !bro->m_right)
                        || ((!bro->m_left || !bro->m_left->m_red) &&
                            (!bro->m_right || !bro->m_right->m_red)))
                {
                    bro->m_red = true;
                    node = node->m_parent;
                    continue;
                }
                else
                {
                    if(!nephew_a->m_red)
                    {
                        nephew_b->m_red = false;
                        bro->m_red = true;
                        is_left ? rot_right(bro):
                                rot_left(bro);
                        continue;
                        //bro = node->m_parent->m_right;
                    }
                    bro->m_red = node->m_parent->m_red;
                    node->m_parent->m_red = false;
                    nephew_a->m_red = false;
                    is_left ? rot_left(node->m_parent):
                            rot_right(node->m_parent);
                    break;
                }
            }

        }
        node->m_red = false;
    }


    inline  rbt_node * find_node(const Key & key) noexcept
    {
        rbt_node * node = m_root;
        while(true)
        {
            if(node->m_data->m_first > key)
            {
                if(node->m_left)
                {
                    node = node->m_left;
                    continue;
                }
                return nullptr;
            }
            else if(node->m_data->m_first < key)
            {
                if(node->m_right)
                {
                    node = node->m_right;
                    continue;
                }
                return nullptr;
            }
            return node;
        }

    }


    inline const rbt_node * find_node(const Key & key) const noexcept
    {
        rbt_node * node = m_root;
        while(true)
        {
            if(node->m_data->m_first > key)
            {
                if(node->m_left)
                {
                    node = node->m_left;
                    continue;
                }
                return nullptr;
            }
            else if(node->m_data->m_first < key)
            {
                if(node->m_right)
                {
                    node = node->m_right;
                    continue;
                }
                return nullptr;
            }
            return node;
        }

    }


    rbt_node * insert_node(const rbt_data & data)
    {
        if(!m_root)
        {
            m_root = new rbt_node(data, nullptr);
            validate_insert(m_root);
            return m_root;
        }
        rbt_node * node = m_root ;
        while(true)
        {
            if(node->m_data->m_first > data->m_first)
            {
                if(node->m_left)
                {
                    node = node->m_left;
                    continue;
                }
                rbt_node * cleft = new rbt_node(data, node);
                node->m_left = cleft;
                validate_insert(cleft);
                node = cleft;
                break;

            }
            else if (node->m_data->m_first < data->m_first)
            {
                if(node->m_right)
                {
                    node = node->m_right;
                    continue;
                }

                rbt_node * cright = new rbt_node(data, node);
                node->m_right = cright;
                validate_insert(cright);
                node = cright;
                break;
            }
            node->m_data = data;
            break;
        }
        return node;
    }

    rb_tree(const rb_tree &) = delete ;
    rb_tree(rb_tree &&) = delete;
    rb_tree & operator = (const rb_tree &) = delete ;
    rb_tree & operator = (rb_tree &&) = delete ;

};//class rb_tree
}//namespace itl
#endif // ITL_RED_BLACK_TREE_H
