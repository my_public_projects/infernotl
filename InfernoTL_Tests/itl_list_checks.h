#ifndef ITL_LIST_CHECKS_H
#define ITL_LIST_CHECKS_H
#include <itl_list.h>
namespace itl
{
namespace list_tests {
namespace checks {


list<std::string> get_string_list(int size)
{
    list<std::string> res;
    while (size) {
        res.push_front("String number:" + std::to_string(size));
        --size;
    }
    return res;
}

void print_list(const list<std::string> & _list)
{
    std::cout <<" Printing list: size = " << _list.size() << "\n";
    for(const std::string & s: _list)
    {
        std::cout << s << "\n";
    }
    std::cout << "List printed!\n";
}

void list_copy_constructor_check()
{
    std::cout << "\n\n"
                 "----itl::list::list(const vector &) check----\n";
    list<std::string> l1 = get_string_list(15);
    list<std::string> l2 = l1;
    std::cout << "Lsit1: ";
    print_list(l1);
    std::cout << "\nList2: ";
    print_list(l2);
    std::cout << "Lists is equal check:" << ( l1 == l2) << "\n";
}

list<std::string>::list_iterrator getIter(list<std::string> &l, int num)
{
    auto begin = l.begin();
    auto end = l.end();
    while(begin != end && num)
    {
        ++begin;
        --num;
    }
    return begin;
}

void list_move_constructor_check()
{
    std::cout << "\n\n"
                 "----itl::list::list(vector &&) check----\n";
    list<std::string> l1 = get_string_list(10);
    list<std::string> l2 = std::move(l1);

    std::cout << "Lsit1: ";
    print_list(l1);
    std::cout << "\nList2: ";
    print_list(l2);
    std::cout << "Lists is equal check:" << ( l1 == l2) << "\n";
}

void list_insert_erase_check()
{
    std::cout << "\n\n"
                 "----itl::list::insert(list_iterator, const T &)) check----\n";
    list<std::string> _list = get_string_list(12);
    std::cout << "List before insert";
    print_list(_list);
    for (ull i = 0; i < 10; ++i)
    {
        _list.insert(getIter(_list, rand() % static_cast<int>(_list.size())), " inserted string " + std::to_string(i));
    }
    std::cout << "\nList after insert";
    print_list(_list);
    std::cout << "\n\n"
                 "----itl::list::erase(list_iterator) check----\n";
    for (ull i = 0; i < 10; ++i)
    {
        _list.erase(getIter(_list, rand() % static_cast<int>(_list.size())));
    }

    std::cout << "\nList after erase";
    print_list(_list);
}

void list_clear_check()
{
    std::cout << "\n\n"
                 "----itl::list::clear() check----\n";
    list<std::string> _list = get_string_list(10);
    std::cout << "list before clear() ";
    print_list(_list);
    _list.clear();
    std::cout << "list after clear ";
    print_list(_list);
    std::cout << "Try push:";
    _list.push_back("pushed string");
    print_list(_list);
    std::cout << "Try insert string";
    for(ull i = 0; i < 5; ++i)
    {
        _list.insert(getIter(_list, rand() % static_cast<int>(_list.size())), " inserted string" + std::to_string(i));
    }
    print_list(_list);

}



}//namespace checks
}//namespace list_tests
}//namespace itl
#endif // ITL_LIST_CHECKS_H
