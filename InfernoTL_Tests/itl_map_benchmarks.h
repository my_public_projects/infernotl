#ifndef ITL_MAP_BENCHMARKS_H
#define ITL_MAP_BENCHMARKS_H
#include <itl_map.h>
#include <map>
#include <chrono>
#include <vector>

namespace itl
{
namespace map_tests {
namespace benchmarks {

void map_insert_benchmark_iterations(long long & itl_time, long long & std_time, int iterations_number)
{
    itl::map<int,int> itl_map;
    std::map<int,int> std_map;
    std::string s = "Hello, my darling!";

    //--------- itl test ---------
    std::chrono::system_clock::time_point itl_start_time;
    itl_start_time = std::chrono::system_clock::now(); // itl start time
    for(int i =0; i < iterations_number; ++i)
    {
        itl_map.insert(i, i+1);
    }

    std::chrono::system_clock::time_point itl_end_time;
    itl_end_time = std::chrono::system_clock::now(); // itl end time

    //--------- std test ---------
    std::chrono::system_clock::time_point std_start_time;
    std_start_time = std::chrono::system_clock::now(); // std start time

    for(int i = 0; i < iterations_number; ++i)
    {
        std_map.insert(std::pair<int, int>(i, i+1));
    }

    std::chrono::system_clock::time_point std_end_time;
    std_end_time = std::chrono::system_clock::now(); // std end time


    // Calculating delta time
    auto itl_delta = itl_end_time - itl_start_time;
    auto std_delta = std_end_time - std_start_time;

    itl_time = std::chrono::duration_cast<std::chrono::nanoseconds>(itl_delta).count();
    std_time = std::chrono::duration_cast<std::chrono::nanoseconds>(std_delta).count();

}


void map_insert_benchmark()
{
    int repeates = 1000;
    int iterations_number = 10000;
    std::cout << "Starting itl::map::insert(const Key &, const Value &) vs. "
                 "std::map::insert(std::pair<Key, Value>&&) speed test\n"
              << repeates << " repeates\n";

    std::cout << iterations_number << " iterations\n";
    std::vector<std::pair<long long, long long>> total;
    for(int i = 0; i < repeates; ++i)
    {
        long long itl_time;
        long long std_time;
        map_insert_benchmark_iterations(itl_time, std_time, iterations_number);
        total.push_back(std::pair<long long, long long>(itl_time, std_time));
    }

    std::cout << "\n******TOTAL******\n"
              << "itl::map<int, int> vs std::map<int, int>\n";

    for(auto & p : total)
    {
        std::string s;
        s += std::to_string(p.first) + " vs " +
            std::to_string(p.second) +
            " lose:" +
            std::to_string((p.first - p.second)) + "\n";
        std::cout << s;
    }

}


void fill_itl_map(map<int, int> & _map, int el_count)
{
    for(int i = 0; i<el_count; ++i)
    {
        _map.insert(i, i+1);
    }
}

void fill_std_map(std::map<int, int> & _map, int el_count)
{
    for(int i = 0; i < el_count; ++i)
    {
        _map.insert(std::pair<int, int>(i, i+1));
    }
}

void map_find_benchmark_iterations(long long & itl_time, long long & std_time, int iterations_number)
{
    itl::map<int,int> itl_map;
    std::map<int,int> std_map;
    std::string s = "Hello, my darling!";

    //--------- itl test ---------

    fill_itl_map(itl_map, iterations_number);
    std::chrono::system_clock::time_point itl_start_time;
    itl_start_time = std::chrono::system_clock::now(); // itl start time
    for(int i =0; i < iterations_number; ++i)
    {
        itl_map.find(rand() % iterations_number);
    }

    std::chrono::system_clock::time_point itl_end_time;
    itl_end_time = std::chrono::system_clock::now(); // itl end time

    //--------- std test ---------
    fill_std_map(std_map, iterations_number);
    std::chrono::system_clock::time_point std_start_time;
    std_start_time = std::chrono::system_clock::now(); // std start time

    for(int i = 0; i < iterations_number; ++i)
    {
        std_map.find(rand() % iterations_number);
    }

    std::chrono::system_clock::time_point std_end_time;
    std_end_time = std::chrono::system_clock::now(); // std end time


    // Calculating delta time
    auto itl_delta = itl_end_time - itl_start_time;
    auto std_delta = std_end_time - std_start_time;

    itl_time = std::chrono::duration_cast<std::chrono::nanoseconds>(itl_delta).count();
    std_time = std::chrono::duration_cast<std::chrono::nanoseconds>(std_delta).count();
}

void map_find_benchmark()
{
    int repeates = 1000;
    int iterations_number = 10000;
    std::cout << "Starting itl::map::find(const Key &) vs. "
                 "std::map::find(const Key &) speed test\n"
              << repeates << " repeates\n";

    std::cout << iterations_number << " iterations\n";
    std::vector<std::pair<long long, long long>> total;
    for(int i = 0; i < repeates; ++i)
    {
        long long itl_time;
        long long std_time;
        map_insert_benchmark_iterations(itl_time, std_time, iterations_number);
        total.push_back(std::pair<long long, long long>(itl_time, std_time));
    }

    std::cout << "\n******TOTAL******\n"
              << "itl::map<int, int> vs std::map<int, int>\n";

    for(auto & p : total)
    {
        std::string s;
        s += std::to_string(p.first) + " vs " +
            std::to_string(p.second) +
            " lose:" +
            std::to_string((p.first - p.second)) + "\n";
        std::cout << s;
    }

}




void map_erase_benchmark_iterations(long long & itl_time, long long & std_time, int iterations_number)
{
    itl::map<int,int> itl_map;

    std::map<int,int> std_map;
    std::string s = "Hello, my darling!";

    //--------- itl test ---------
    fill_itl_map(itl_map, iterations_number);
    std::chrono::system_clock::time_point itl_start_time;
    itl_start_time = std::chrono::system_clock::now(); // itl start time
    for(int i =0; i < iterations_number; ++i)
    {
        itl_map.erase(i);
    }

    std::chrono::system_clock::time_point itl_end_time;
    itl_end_time = std::chrono::system_clock::now(); // itl end time

    //--------- std test ---------

    fill_std_map(std_map, iterations_number);
    std::chrono::system_clock::time_point std_start_time;
    std_start_time = std::chrono::system_clock::now(); // std start time

    for(int i = 0; i < iterations_number; ++i)
    {
        auto it = std_map.find(i);
        std_map.erase(it);
    }

    std::chrono::system_clock::time_point std_end_time;
    std_end_time = std::chrono::system_clock::now(); // std end time


    // Calculating delta time
    auto itl_delta = itl_end_time - itl_start_time;
    auto std_delta = std_end_time - std_start_time;

    itl_time = std::chrono::duration_cast<std::chrono::nanoseconds>(itl_delta).count();
    std_time = std::chrono::duration_cast<std::chrono::nanoseconds>(std_delta).count();

}

void map_erase_benchmark()
{
    int repeates = 1000;
    int iterations_number = 10000;
    std::cout << "Starting itl::map::erase(const Key &) vs. "
                 "std::map::erase(iterator ) speed test\n"
              << repeates << " repeates\n";

    std::cout << iterations_number << " iterations\n";
    std::vector<std::pair<long long, long long>> total;
    for(int i = 0; i < repeates; ++i)
    {
        long long itl_time;
        long long std_time;
        map_insert_benchmark_iterations(itl_time, std_time, iterations_number);
        total.push_back(std::pair<long long, long long>(itl_time, std_time));
    }

    std::cout << "\n******TOTAL******\n"
              << "itl::map<int, int> vs std::map<int, int>\n";

    for(auto & p : total)
    {
        std::string s;
        s += std::to_string(p.first) + " vs " +
            std::to_string(p.second) +
            " lose:" +
            std::to_string((p.first - p.second)) + "\n";
        std::cout << s;
    }

}


}//namespace benchmarks
}//namespace map_tests
}//namespace itl
#endif // ITL_MAP_BENCHMARKS_H
