# InfernoTL

My teamplate library. Why? For more exp.  
Realized structs:  
 - **vector\<T\>,**   
 - **list\<T\>**,  
 - **map\<T1, T2\>**

 To try it, do next:  
  1. Clone this project **$git clone https://gitlab.com/my_public_projects/infernotl.git**  
  2. Insert in your project include path **infernotl/include/**
  3. **Done!!!**  

To get the documentation of code, generate it with command  
    **$doxygen doxy_conf**  
in *infernotl* dirrectory  

Also, you can look at some benchmarks in  **infernotl/InfernoTL_Tests/** dirrectory
