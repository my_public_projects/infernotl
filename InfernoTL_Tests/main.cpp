#include <itl_defines.h>
#include <itl_debug_dummy.h>

#include <itl_memcopy_benchmark.h>
#include <itl_vector_benchmarks.h>
#include <itl_vector_checks.h>
#include <itl_map_checks.h>
#include <itl_list_checks.h>
#include <itl_map_benchmarks.h>

int main()
{
    //itl::memory_test::mem_copy_benchmark();
    //itl::vector_tests::checks::vector_push_back_test();
    //itl::vector_tests::checks::vector_copy_constructor_check();
    //itl::vector_tests::checks::vector_move_constructor_check();
    //itl::vector_tests::checks::vector_operator_assignment_check();
    //itl::vector_tests::checks::vector_operator_move_assignment_check();
    //itl::vector_tests::checks::vector_operator_equal_check();
    //itl::vector_tests::checks::vector_begin_end_check();
    //itl::vector_tests::checks::vector_resize_check();
    //itl::vector_tests::checks::vector_insert_check();
    //itl::vector_tests::checks::vector_erase_check();



    //itl::vector_tests::benchmarks::vector_push_back_benchmark();
    //itl::vector_tests::benchmarks::vector_isert_benchmark();


    //itl::map_tests::checks::erase_check();

    //itl::map_tests::benchmarks::map_insert_benchmark();
    //itl::map_tests::benchmarks::map_find_benchmark();
    itl::map_tests::benchmarks::map_erase_benchmark();

//    itl::list_tests::checks::list_copy_constructor_check();
//    itl::list_tests::checks::list_move_constructor_check();
//    itl::list_tests::checks::list_insert_erase_check();
//    itl::list_tests::checks::list_clear_check();
    return 0;
}
