#ifndef VECTOR_H
#define VECTOR_H
#include "itl_defines.h"
#include <stdexcept>
namespace itl {

/*!
 * \brief Template class vector
 * \author InfernalBoy
 * \details This class is analog of std::vector. Why? For more experience!
 * T must meet the requirements of CopyAssignable, MoveAssignable, MoveConstructible.
 */

template<typename T, ull default_capacity = 16ull>
class vector{

    char * m_data = new char[(default_capacity * sizeof (T))];
    ull m_used = 0ull;
    ull m_allocated = (default_capacity * sizeof (T));
private:


    /*!
     * \brief call_destructors() noexcept
     * \details Call destructors of elements
     */
    void call_destructors() noexcept
    {
        T * cursor = reinterpret_cast<T*>(m_data);
        T * const last = reinterpret_cast<T*>(m_data) + size() - 1;
        while(cursor <= last)
        {
            (cursor++)->~T();
        }
    }


    /*!
     * \brief allocate(ull)
     * \param count [in] The number of bytes needed for allocating.
     * \details Destroys all elements and allocating buffer with size = m_allocated + count.
     */
    void allocate(ull count) noexcept
    {
        if(m_used != 0)
        {
            call_destructors();
        }
        delete [] m_data;

        m_allocated += count;
        m_data = new char[m_allocated];
        m_used = 0ull;
    }


    /*!
     * \brief realloc()
     * \details Reallocation of memory
     */
    void realloc()
    {
        // Calculating new size
        ull temp_size = (m_allocated) ? m_allocated * 2 : default_capacity * sizeof (T);

        // Allocating a new buffer
        char * new_data = new char[temp_size];

        // Copying form m_data to the new_data buffer
        ull old_size = size();

        T * cursor = reinterpret_cast<T*>(m_data);
        T  * const last = reinterpret_cast<T*>(m_data) + old_size - 1;
        T * dest_t = reinterpret_cast<T*>(new_data);

        while (cursor <= last)
        {
            new  (dest_t++) T(std::move(*cursor)); //Copy element to new memory.
            cursor->~T(); // Call destructor of element from old buffer.
            cursor++;
        }
        // Replace pointer to the old data with the pointer to the new data.
        delete [] m_data;
        m_data = new_data;
        m_allocated = temp_size;

    }

public:

    /*!
     * \brief vector() - Creates an empty vector.
     * \details  An empty vector allocates memory for default_capaity = 16 elements.
     */
    vector()
    {
    }


    /*!
     * \brief vector(const vector & a)
     * \param a [in] The vector for copy from.
     * \details Creates a copy of the vector a.
     */
    vector(const vector & a)
    {
        if(a.m_allocated > m_allocated)
        {
            allocate(a.m_allocated - m_allocated);
        }

        T * cursor = reinterpret_cast<T*>(a.m_data);
        T * const last = reinterpret_cast<T*>(a.m_data) + a.size() - 1;
        T * dest = reinterpret_cast<T*>(m_data);

        while(cursor <= last)
        {
            new (dest++) T(*(cursor++));
        }

        m_used = a.m_used;

    }


    /*!
     * \brief vector(vector && a) noexcept
     * \param a [in] The vector to move from.
     * \details Move constructor.
     */
    vector(vector && a) noexcept
    {
        ull temp = m_used;
        m_used = a.m_used;
        a.m_used = temp;

        char * temp_mem = m_data;
        m_data = a.m_data;
        a.m_data = temp_mem;

        temp = m_allocated;
        m_allocated = a.m_allocated;
        a.m_allocated = temp;
    }


    /*!
     * \brief Assignment operator.
     * \param a [in] The vector for copy from.
     * \return Reference to self.
     */
    vector & operator=(const vector & a)
    {
        if(a.m_allocated > m_allocated)
        {
            allocate(a.m_allocated - m_allocated);
        }
        else
        {

            call_destructors();
        }

        T * cursor = reinterpret_cast<T*>(a.m_data);
        T * const last = reinterpret_cast<T*>(a.m_data) + a.size() - 1;
        T * dest = reinterpret_cast<T*>(m_data);

        while(cursor <= last)
        {
            new (dest++) T(*(cursor++));
        }

        m_used = a.m_used;

        return *this;

    }


    /*!
     * \brief Move assignment operator
     * \param a [in] The vector to move from
     * \return Reference to self.
     */
    vector & operator=(vector && a) noexcept
    {
        ull temp = m_used;
        m_used = a.m_used;
        a.m_used = temp;

        char * temp_mem = m_data;
        m_data = a.m_data;
        a.m_data = temp_mem;

        temp = m_allocated;
        m_allocated = a.m_allocated;
        a.m_allocated = temp;
        return  *this;
    }


    /*!
     * \brief Destructor.
     * \details Destroy the all elements and free memory.
     */
    ~vector()
    {
        if(m_used != 0)
        {
            call_destructors();
        }
        delete [] m_data;
    }


    /*!
     * \brief push_back(const T &)
     * \param elem [in] Element to add.
     * \details Append element to the back. If a new element does not fit into the current buffer, then reallocation occurs.
     */
    void push_back(const T & elem)
    {
        ull used_t = m_used + sizeof (T);
        if(used_t > m_allocated)
        {
            realloc();
        }
        T * temp = (reinterpret_cast<T*>((m_data + (m_used))) );
        new (temp) T(elem);
        m_used = used_t;

    }


    /*!
     * \brief push_back(T &&)
     * \param elem [in] Element to add.
     * \details Append element to the back. If a new element does not fit into the current buffer, then reallocation occurs.
     */
    void push_back(T && elem)
    {
        ull used_t = m_used + sizeof (T);
        if(used_t > m_allocated)
        {
            realloc();
        }
        T * temp = (reinterpret_cast<T*>((m_data + (m_used))) );
        new (temp) T(std::move(elem));
        m_used = used_t;

    }


    template<typename ... Args>
    /*!
     * \brief emplace_back(Args && ...) noexcept
     * \tparam args [in] Arguments for call T(args...)
     * \details Emplace_back create new element to back.
     */
    void emplace_back(Args && ... args) noexcept
    {
        ull used_t = m_used + sizeof (T);
        if(used_t > m_allocated)
        {
            realloc();
        }
        T * temp = (reinterpret_cast<T*>((m_data + (m_used))) );
        new (temp) T(args...);
        m_used = used_t;

    }


    /*!
     * \brief Removing last element from vector.
     * \return Last element.
     */
    T pop_back()
    {
        T * temp = reinterpret_cast<T*>(m_data + m_used - sizeof (T));
        T result = *temp;
        temp->~T();
        m_used -= sizeof (T);
        return  result;
    }


    /*!
     * \brief Access to element by index.
     * \param index [in] Elment's index in vector.
     * \return Reference to element.
     * \details Checks index.
     * \throw std::out_of_range if the index is outside the vector
     */
    T & at(ull index)
    {
        if(index > size()-1) {
            throw std::out_of_range(__FUNCTION__ + std::string("->Out of range")); //TODO How about a itl::exception?
        }
        T * temp = reinterpret_cast<T*>(m_data) + index;
        return *temp;
    }


    /*!
     * \brief Access to the const element by index.
     * \param index [in] Elment's index in vector.
     * \return Reference to the const element.
     * \details Checks index.
     * \throw std::out_of_range if the index is outside the vector
     */
   const T & at(ull index) const
    {
        if(index > size()-1) {
            throw std::out_of_range(__FUNCTION__ + std::string("->Out of range")); //TODO How about a itl::exception?
        }
        T * temp = reinterpret_cast<T*>(m_data) + index;
        return *temp;
    }


    /*!
     * \brief operator [] Give access to the element by index.
     * \param index [in]  Index.
     * \return Refernce to the element.
     * \details Without checking index.
     */
    T & operator[](ull index)
    {
        T * temp = reinterpret_cast<T*>(m_data) + index;
        return *temp;
    }


    /*!
     * \brief operator [] const Give access to the const element by index.
     * \param index [in]  Index.
     * \return Refernce to the const element.
     */
    const T & operator[](ull index) const
    {
        T * temp = reinterpret_cast<T*>(m_data) + index;
        return *temp;
    }


    /*!
     * \brief operator == checks two vectors to equality.
     * \param b [in] Vector for check.
     * \return true if all elements of two vectors is equal. Else return false.
     */
    bool operator==(const vector &b) const
    {
        if(m_used != b.m_used)
        {
            return false;
        }
        const T * b_data = reinterpret_cast<const T*>(b.m_data);
        const T * my_data = reinterpret_cast<const T *>(m_data);
        const T * const last = reinterpret_cast<const T *>(m_data) + size()-1;
        while (my_data <= last) {
            if(!(*(my_data++) == *(b_data++))) return false;
        }
        return true;
    }


    /*!
     * \brief operator != checks two vectors to not equality.
     * \param b [in] Vector for check.
     * \return true if !(*this == b)
     */
    bool operator!=(const vector & b)
    {
        if(m_used != b.m_used)
        {
            return true;
        }

        const T * b_data = reinterpret_cast<const T*>(b.m_data);
        const T * my_data = reinterpret_cast<const T *>(m_data);
        const T * const last = reinterpret_cast<const T *>(m_data) + size()-1;
        while (my_data <= last) {
            if(!(*(my_data++) == *(b_data++))) return true;
        }
        return false;

    }


    /*!
     * \brief empty()
     * \return True if vector has not elements
     */
    inline bool empty()
    {
        return  !m_used;
    }

    //TODO How about a itl::iterator? :)

    /*!
     * \brief begin()
     * \return pointer to the first element.
     */
    inline T * begin()
    {
        return reinterpret_cast<T*>(m_data);
    }

    /*!
     * \brief begin() const
     * \return pointer to the const first element.
     */
    inline const T * begin() const
    {
        return reinterpret_cast<T*>(m_data);
    }

    /*!
     * \brief cbegin() const
     * \return pointer to the const first element.
     */
    inline const T * cbegin() const
    {
        return reinterpret_cast<T*>(m_data);
    }

    /*!
     * \brief end()
     * \return pointer to the next after last element.
     */
    inline T * end()
    {
        return reinterpret_cast<T*>(m_data) + size();
    }

    /*!
     * \brief end() const
     * \return pointer to the const next after last element.
     */
    inline const T * end() const
    {
        return reinterpret_cast<T*>(m_data) + size();
    }

    /*!
     * \brief cend() const
     * \return pointer to the const next after last element.
     */
    inline const T * cend() const
    {
        return reinterpret_cast<T*>(m_data) + size();
    }


    /*!
     * \brief Returns count of vector's elements.
     */
    inline ull size() const noexcept {
        return m_used / sizeof (T);
    }


    /*!
     * \brief swap() exchanges values with b.
     * \param b [in] Vector for exchange with.
     */
    void swap(vector & b) noexcept
    {
        ull temp = m_used;
        m_used = b.m_used;
        b.m_used = temp;

        char * temp_mem = m_data;
        m_data = b.m_data;
        b.m_data = temp_mem;

        temp = m_allocated;
        m_allocated = b.m_allocated;
        b.m_allocated = temp;
    }


    /*!
     * \brief max_size() const
     * \return max possible count of elements
     */
    inline ull max_size() const noexcept
    {
        ull res = static_cast<ull>(-1);
        res = res / sizeof (T);
        return res;
    }


    /*!
     * \brief front()
     * \return Reference to the first element.
     */
    T & front()
    {
        return *reinterpret_cast<T*>(m_data);
    }


    /*!
     * \brief front() const
     * \return Reference to the const first element.
     */
    const T & front() const
    {
        return *reinterpret_cast<T*>(m_data);
    }


    /*!
     * \brief back()
     * \return Reference to the last element.
     */
    T & back()
    {
        return *(reinterpret_cast<T*>(m_data) + size()-1);
    }


    /*!
     * \brief back() const
     * \return Reference to the const last element.
     */
    const T & back() const
    {
        return *(reinterpret_cast<T*>(m_data) + size()-1);
    }


    /*!
     * \brief data()
     * \return Pointer to the first element storage.
     */
    T * data()
    {
        return reinterpret_cast<T*>(m_data);
    }


    /*!
     * \brief data() const
     * \return Pointer to the const first element storage.
     */
    const T * data() const
    {
        return reinterpret_cast<T*>(m_data);
    }

    /*!
     * \brief capacity() const
     * \return Length of allocated storage.
     */
    inline ull capacity() const noexcept
    {
        return  m_allocated / sizeof (T);
    }


    /*!
     * \brief reserve()
     * \param _size [in] Elements count.
     * \details Reserve memory for _size elements. But no less than default_capacity.
     */
    void reserve(ull _size) noexcept
    {
        ull size_in_bytes = _size * sizeof (T);
        if(size_in_bytes > m_allocated) {
            allocate(size_in_bytes);
        }
    }


    /*!
     * \brief shrink_to_fit()
     * \details Set capacity = size(). Reallocation occurs.
     */
    void shrink_to_fit() noexcept
    {
        ull temp_size = size();

        if(0 == temp_size)
        {
            temp_size = default_capacity;
        }

        ull fit_size = temp_size * sizeof (T);

        char * new_data = new char[fit_size];

        T * new_data_t = reinterpret_cast<T*>(new_data);
        T * current_data_t = reinterpret_cast<T*>(m_data);
        T * const last = current_data_t + temp_size - 1;

        while (current_data_t <= last) {
            new (new_data_t++) T(*current_data_t);
            current_data_t->~T();
            current_data_t++;
        }

        delete [] m_data;
        m_data = new_data;
        m_allocated = temp_size;
    }


    /*!
     * \brief clear()
     * \details Destroys all elements. Capacity is not changing.
     */
    void clear() noexcept
    {
        if(m_used)
        {
            call_destructors();
            m_used = 0;
        }
    }


    /*!
     * \brief resize(ull)
     * \param elem_count [in] number of elements.
     * \details Resizes the vector to contain count elements.
     */
    void resize(ull elem_count)
    {
        ull current_size = size();
        if(elem_count == current_size)
        {
            return;
        }

        if(elem_count > current_size)
        {
            ull delta = elem_count - current_size;
            for(ull i = 0 ; i < delta; ++i)
            {
                emplace_back();
            }
        }
        else
        {
            ull delta = current_size - elem_count;

            T * const data_t = reinterpret_cast<T*>(m_data);

            T * elem_to_destroy = data_t + delta;
            T * const last = data_t + current_size - 1;

            while(elem_to_destroy <= last)
            {
                (elem_to_destroy++)->~T();
            }
            m_used -= delta * sizeof (T);
        }
    }


    /*!
     * \brief insert(ull, const T&)
     * \param pos [in] position to insert.
     * \param value [in] value to insert.
     * \return Pointer to the inserted element
     */
    T * insert(ull pos, const T & value)
    {
        ull _size = size();

        if(pos >= _size)
        {
            resize(pos + 1);
            T & res = back();
            res = value;
            return &res;
        }

        emplace_back();

        T * const data = reinterpret_cast<T*>(m_data);
        T * const insert_pos = data + pos;
        T * new_element = data + _size;
        T * temp;
        while(new_element > insert_pos)
        {
            temp = new_element--;
            *(temp) = *new_element;
        }

        *insert_pos = value;
        return insert_pos;
    }


    /*!
     * \brief insert(ull, T&&)
     * \param pos [in] position to insert.
     * \param value [in] value to insert.
     * \return Pointer to the inserted element
     */
    T * insert(ull pos, T && value)
    {
        ull _size = size();

        if(pos >= _size)
        {
            resize(pos + 1);
            T & res = back();
            res = value;
            return &res;
        }

        emplace_back();

        T * const data = reinterpret_cast<T*>(m_data);
        T * const insert_pos = data + pos;
        T * new_element = data + _size;
        T * temp;
        while(new_element > insert_pos)
        {
            temp = new_element--;

            *temp = std::move(*new_element);
        }

        *insert_pos = std::move(value);
        return insert_pos;
    }


    /*!
     * \brief erase(ull)
     * \param pos [in] index of the removing element.
     */
    void erase(ull pos)
    {
        ull _size = size();

        if( pos >= _size)
        {
            throw std::out_of_range("pos >= size");
        }

        T * const data_t = reinterpret_cast<T*>(m_data);
        T * const last = data_t + _size -1;
        T * erase_elem = data_t + pos;
        T * temp;
        while(erase_elem < last)
        {
            temp = erase_elem++;
            *temp = std::move(*erase_elem);
        }
        last->~T();
        m_used -= sizeof (T);
    }


    /*!
     * \brief erase(ull, ull)
     * \param pos [in] index of the first removing element.
     * \param count [in] count of removing elements.
     */
    void erase(ull pos, ull count)
    {
        ull _size = size();

        if( pos >= _size || pos + count >= _size || !count)
        {
            throw std::out_of_range("pos or count is wrong");
        }

        T * const data_t = reinterpret_cast<T*>(m_data);
        T * const last = data_t + _size -1;
        T * pos_elem = data_t + pos;
        T * tail_elem = pos_elem + count;

        while(tail_elem <= last)
        {
            *(pos_elem++) = std::move(*(tail_elem++));
        }

        while(pos_elem <= last)
        {
            (pos_elem++)->~T();
        }

        m_used -= sizeof (T) * count;
    }
};//class vector
}//namespace itl
#endif // VECTOR_H
