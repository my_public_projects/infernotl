#ifndef ITL_VECTOR_TESTS_H
#define ITL_VECTOR_TESTS_H
#include <itl_vector.h>
#include <vector>
#include <string>
#include <chrono>
#include <iostream>

namespace itl
{
namespace vector_tests
{
/*!
 * \brief namespace for tests
 */
namespace checks {


/*!
 * \brief vector_copy_constructor_check()
 * \details Checks workable of itl::vector::vector(const vector &)
 */
void vector_copy_constructor_check()
{
    std::cout << "\n\n"
                 "----itl::vector::vector(const vector &) check----\n";
    vector<int> vec_a;
    for(int i = 0; i < 5; ++i)
    {
        vec_a.push_back(rand());
    }

    vector<int> vec_b(vec_a);
    while(vec_b.size())
    {
        std::cout << vec_b.pop_back() << " vs " << vec_a.pop_back() << "\n";
    }
}

/*!
 * \brief get_random_vector_int(ull)
 * \param elem_num [in] size of returned vector.
 * \return Vector filled of random numbers.
 */
vector<int> get_random_vector_int(ull elem_num)
{
    vector<int> res;
    for(ull i = 0; i < elem_num; ++i)
    {
        res.push_back(rand());
    }
    return res;
}

/*!
 * \brief get_random_vector_str(ull)
 * \param elem_num [in] size of returned vector.
 * \return Vector filled of random strings.
 */
vector<std::string> get_random_vector_str(ull elem_num)
{
    vector<std::string> res;
    for(ull i = 0; i < elem_num; ++i)
    {
        res.push_back("string " + std::to_string(rand()));
    }
    return res;
}

/*!
 * \brief get_escalating_vector_int(ull)
 * \param elem_num [in] size of returned vector.
 * \return Vector filled of the escalating sequence of numbers.
 */
vector<int> get_escalating_vector_int(ull elem_num)
{
    vector<int> res;
    for(ull i = 0; i < elem_num; ++i)
    {
        res.push_back(static_cast<int>(i));
    }
    return res;
}

/*!
 * \brief get_escalating_vector_str(ull)
 * \param elem_num [in] size of returned vector.
 * \return Vector filled of the strings with the escalating numeration.
 */
vector<std::string> get_escalating_vector_str(ull elem_num)
{
    vector<std::string> res;
    for(ull i = 0; i < elem_num; ++i)
    {
        res.push_back("string " + std::to_string(i));
    }
    return res;
}


/*!
 * \brief vector_move_constructor_check()
 * \details Check workable of itl::vector::vector(vector &&).
 */
void vector_move_constructor_check()
{
    std::cout << "\n\n"
                 "----itl::vector::vector(vector &&) check----\n";
    vector<int> vec_a = get_random_vector_int(25);
    vector<int> vec_b = std::move(vec_a);

    ull size = vec_b.size();
    for(ull i = 0; i < size; ++i)
    {
        std::cout << vec_b[i] << "\n";
    }
}


/*!
 * \brief vector_operator_assignment_check()
 * \details Check workable of itl::vector::operator=(const vector &).
 */
void vector_operator_assignment_check()
{
    std::cout << "\n\n"
                 "----itl::vector::operator=(const vector &) check----\n";
    vector<int> vec_a = get_random_vector_int(25);
    vector<int> vec_b = get_random_vector_int(33);
    vec_a = vec_b;
    ull size_a = vec_a.size();
    for(ull i = 0; i < size_a; ++i)
    {
        std::cout << vec_a[i] << " vs " << vec_b[i] << "\n";
    }
}

/*!
 * \brief vector_operator_move_assignment_check()
 * \details Check workable of itl::vector::operator=(vector &&).
 */
void vector_operator_move_assignment_check()
{
    std::cout << "\n\n"
                 "----itl::vector::operator=(vector &&) check----\n";
    vector<int> vec_a = get_random_vector_int(24);
    vector<int> vec_b = get_random_vector_int(34);
    vector<int> vec_c = get_random_vector_int(44);
    vec_a = std::move(vec_b);
    vec_c = std::move(vec_a);
    std::cout << "a:" << vec_a.size() << "\n";
    std::cout << "b:" << vec_b.size() << "\n";
    std::cout << "c:" << vec_c.size() << "\n";
}

/*!
 * \brief vector_operator_equal_check()
 * \details Check workable of itl::vector::operator==(const vector &).
 */
void vector_operator_equal_check()
{
    std::cout << "\n\n"
                 "----itl::vector::operator==(const vector &) check----\n";
    vector<int> vec_a = get_random_vector_int(10);
    vector<int> vec_b = vec_a;
    vector<int> vec_c = get_random_vector_int(10);

    std::cout << " Vector equal check is " << (vec_a == vec_b) << "\n";
    std::cout << " Not equal vectors check is " << (vec_a == vec_c) << "\n";


}

/*!
 * \brief vector_begin_end_check()
 * \details Check workable of itl::vector::begin() and itl::vector::end().
 */
void vector_begin_end_check()
{
    std::cout << "\n\n"
                 "----itl::vector::begin() & itl::vector::end() check----\n";
    const vector<int> vec = get_random_vector_int(10);
    for(const int & e:vec)
    {
        std::cout << e << "\n";
    }

}

/*!
 * \brief vector_push_back_test()
 * \details Check workable of itl::vector::push_back(const T &) and itl::vector::push_back(T &&) .
 */
void vector_push_back_test()
{
    std::cout << "\n\n"
                 "----itl::vector::push_back(const T &) check----\n";
    std::string s = "Hello, my darling!";
    itl::vector<std::string> vec;
    for(int i = 0; i < 50; ++i)
    {
        vec.push_back(s + std::to_string(i));
    }

    for(itl::ull i = 0; i < 50; ++i)
    {
        std::string temp = vec[i];
        std::cout << temp << "\n";
    }


    std::cout << "\n\n"
                 "----itl::vector::push_back(T &&) check----\n";
    itl::vector<std::string> vec_m;
    for(int i = 0; i < 50; ++i)
    {
        std::string temp = s + std::to_string(i);
        vec_m.push_back(std::move(temp));
    }

    for(itl::ull i = 0; i < 50; ++i)
    {
        std::string temp = vec_m[i];
        std::cout << temp << "\n";
    }


}

/*!
 * \brief vector_resize_check()
 * \details Check workable of itl::vector::resize(ull).
 */
void vector_resize_check()
{
    std::cout << "\n\n"
                 "----itl::vector::resize(ull) check---\n"
                 "before resize(15)\n";
    vector<int> vec = get_random_vector_int(10);
    for(const int & e:vec)
    {
        std::cout << e << "\n";
    }

    vec.resize(15);
    std::cout << "\nafter resize(15)\n";
    for(const int & e:vec)
    {
        std::cout << e << "\n";
    }

    vec.resize(5);
    std::cout << "\nafter resize(5)\n";
    for(const int & e:vec)
    {
        std::cout << e << "\n";
    }

    vec.resize(12);
    std::cout << "\nafter resize(12)\n";
    for(const int & e:vec)
    {
        std::cout << e << "\n";
    }

}

/*!
 * \brief vector_insert_check()
 * \details Check workable of itl::vector::insert(const T &) and itl::vector::insert(T &&).
 */
void vector_insert_check()
{
    std::cout << "\n\n"
                 "----itl::vector::insert(int, const T &) check----\n";
    vector<int> vec = get_escalating_vector_int(10);


    vec.insert(3, 12);
    for(int i:vec)
    {
        std::cout << i << "\n";
    }

    vec.insert(15, 27);

    for(int i:vec)
    {
        std::cout << i << "\n";
    }


    std::cout << "\n\n"
                 "----itl::vector::insert(int,T &&) check----\n";

    vector<std::string> vec_s = get_escalating_vector_str(10);


    std::string insert_data("insert_data");
    vec_s.insert(4, std::move(insert_data));
    std::cout << "insert_data after insert:" << insert_data << "\n";
    for(auto & s:vec_s)
    {
        std::cout << s << "\n";
    }
    std::cout << "if (pos > size()):\n";
    insert_data = "overflow_data";
    vec_s.insert(15, std::move(insert_data));

    for(auto & s:vec_s)
    {
        std::cout << s << "\n";
    }

}


/*!
 * \brief vector_erase_check()
 * \details Check workable of itl::vector::erase(ull) and itl::vector::erase(ull, ull).
 */
void vector_erase_check()
{
    std::cout << "\n\n"
                 "----itl::vector::erase(ull) check----\n";

    vector<std::string> vec = get_escalating_vector_str(10);
    vec.erase(5);
    for(auto & s:vec)
    {
        std::cout << s << "\n";
    }

    try {
        vec.erase(15);
    } catch (const std::out_of_range & e) {
        std::cout << " Exception check passed:" << e.what() << "\n";
    }

    std::cout << "\n\n"
                 "----itl::vector::erase(ull, ull) check----\n";
    vec.erase(3, 3);
    for(auto & s:vec)
    {
        std::cout << s << "\n";
    }

    try {
        vec.erase(1, 15);
    } catch (const std::out_of_range & e) {
        std::cout << " Exception check passed:" << e.what() << "\n";
    }
}

}//namespace checks


}// namespace vector_test
}// namespace itl
#endif // ITL_VECTOR_TESTS_H
