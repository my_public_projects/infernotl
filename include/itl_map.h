#ifndef ITL_MAP_H
#define ITL_MAP_H
#include "itl_list.h"
#include "itl_red_black_tree.h"
#include "itl_defines.h"
#include <assert.h>


namespace itl {
template <typename Key, typename Value>
/*!
 * \brief The template map class.
 * \details This class is analog of std::map. Why? For more exp.
 *  Key and Value must meet the requirements of CopyAssignable, MoveAssignable, MoveConstructible.
 * \author InfernalBoy
 */
class map
{


    list<pair<Key, Value>> m_storage;

    rb_tree<Key, Value> * m_tree = nullptr;


public:
    /*!
     * \brief map_iterator is list<pair<Key, Value>>::list_iterrator
     */
    using map_iterator = typename list<pair<Key, Value>>::list_iterrator;


    /*!
     * \brief map() constructs the emty map
     */
    map()
        : m_tree(new rb_tree<Key, Value>())
    {

    }


    /*!
     * \brief map(const map &)
     * \param r [in]
     * \details CopyConstructor
     */
    map(const map & r)
        : m_tree(new rb_tree<Key, Value>())
    {
        for(const pair<Key, Value> & p:r.m_storage)
        {
            insert(p.m_first, p.m_second);
        }
    }


    /*!
     * \brief map(map &&)
     * \param r [in]
     * \details MoveConstructor
     */
    map(map && r)
        : m_storage(std::move(r.m_storage)),
          m_tree(r.m_tree)
    {
        r.m_tree = nullptr;
    }


    /*!
     * \brief operator=(const map &)
     * \param r [in]
     * \return reference to self
     * \details Copy Assignment operator
     */
    map & operator=(const map & r)
    {
        clear();
        for(const pair<Key, Value> & p:r.m_storage)
        {
            insert(p.m_first, p.m_second);
        }
        return *this;
    }


    /*!
     * \brief operator=(map &&)
     * \param r [in]
     * \return reference to self
     * \details Move assignment operator
     */
    map & operator=(map && r)
    {
        m_storage = std::move(r.m_storage);
        auto temp = m_tree;
        m_tree = r.m_tree;
        r.m_tree = temp;
        return *this;
    }

    /*!
     * \brief ~map() destroys the container and frees memory
     */
    ~map()
    {
        delete m_tree;
        m_tree = nullptr;
    }


    /*!
     * \brief insert(const Key &, const Value &)
     * \param key [in]
     * \param value [in]
     * \return map_iterator to inserted element
     * \details Inserts new pair to the container
     */
    map_iterator insert( const Key & key, const Value & value) noexcept
    {
        m_storage.push_back(pair<Key, Value>(key, value));
        auto res = m_storage.end();
        --res;
        m_tree->insert(res);
        return res;

    }


    /*!
     * \brief erase(const Key &)
     * \param key [in]
     * \return map_iterator to the next after erased element. if element is not found return empty map_iterator
     * \details Erases the element with key
     */
    inline map_iterator erase(const Key & key) noexcept
    {
        auto temp = m_tree->erase(key);
        return m_storage.erase(temp ? temp: m_storage.end());
    }


    /*!
     * \brief print
     */
    void print()
    {
        m_tree->print();
    }



    /*!
     * \brief find(const Key &)
     * \param key [in]
     * \return map_iterator to the founded elment. If the element with that key is not found returns empty iterator
     */
    map_iterator find(const Key & key) noexcept
    {
        return m_tree->find(key);
    }


    /*!
     * \brief find(const Key &) const
     * \param key [in]
     * \return const map_iterator to the founded elment. If the element with that key is not found returns empty iterator
     */
    const map_iterator find(const Key & key) const noexcept
    {
        return m_tree->find(key);
    }

    /*!
     * \brief at(const Key &)
     * \param key [in]
     * \return reference to Value by key.
     * \details Gives access to the element by key with index check.
     * \exception std::out_of_range if data with that key is not exists
     */
    Value & at(const Key & key)
    {
        map_iterator res = find(key);
        if(!res)
        {
            throw std::out_of_range("Element not found");
        }
        return res;
    }


    /*!
     * \brief at(const Key &) const
     * \param key [in]
     * \return reference to const Value by key.
     * \details Gives access to the element by key with index check.
     * \exception std::out_of_range if data with that key is not exists
     */
    const Value & at(const Key & key) const noexcept
    {
        map_iterator res = find(key);
        if(!res)
        {
            throw std::out_of_range("Element not found");
        }
        return res;
    }

    /*!
     * \brief operator[](const Key &)
     * \param key [in]
     * \return retuns reference to Value by key.
     * \details Gives access to the element by key index check. If elemnt with key is not found it will be created
     */
    Value & operator[](const Key & key) noexcept
    {
        map_iterator res = find(key);
        if(res)
        {
            return  res->m_second;
        }

        return insert(key,  Value())->m_second;
    }

    /*!
     * \brief clear() - Clears the container
     *
     */
    void clear() noexcept
    {
        m_storage.clear();
        m_tree->clear();
    }


    /*!
     * \brief begin()
     * \return map_iterator to the begin of container
     */
    inline map_iterator begin() noexcept
    {
        return m_storage.begin();
    }

    /*!
     * \brief begin() const
     * \return const map_iterator to the begin of container
     */
    inline const  map_iterator begin() const noexcept
    {
        return m_storage.begin();
    }


    /*!
     * \brief end()
     * \return map_iterator to the end of container
     */
    inline map_iterator end() noexcept
    {
        return m_storage.end();
    }


    /*!
     * \brief end() const
     * \return const map_iterator to the end of container
     */
    inline const map_iterator end() const noexcept
    {
        return m_storage.end();
    }


    /*!
     * \brief size()
     * \return Count of elements
     */
    inline ull size() const noexcept
    {
        return m_storage.size();
    }

    /*!
     * \brief empty()
     * \return true if the container has not elements
     */
    inline bool empty() const noexcept
    {
        return m_storage.empty();
    }

};
} //namespace itl
#endif // ITL_MAP_H
