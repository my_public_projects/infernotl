#ifndef ITL_MAP_CHECKS_H
#define ITL_MAP_CHECKS_H
#include "itl_map.h"
namespace itl {
namespace map_tests {
namespace checks {



void fill_map(map<int, std::string> & _map, int item_count)
{
    for(int i = 0; i < item_count; ++i)
    {

        _map.insert(i+1, "string " + std::to_string(i+1));
    }
}

map<int, std::string> get_map(int item_count)
{
    map<int, std::string> res;
    for(int i = 0; i < item_count; ++i)
    {

        res.insert(i+1, "string " + std::to_string(i+1));
    }
    return res;
}

void erase_check()
{
    std::cout << "\n\n"
                 "----itl::map::erase(const Key &) check----\n";

    int n = 15;
    map<int,std::string> _map = get_map(n);
    std::cout << "\nmap before erase\n";
    _map.print();
   while(_map.size())
    {
       int t = rand() % n+1;
       while (!_map.find(t)) {
           t = rand() % n;
       }
        _map.erase(t);
        std::cout << "\n\nmap after erase element " << t << "\n";
        _map.print();

    }

    std::cout << "filling map after erase\n\n";
    fill_map(_map, n);
    _map.print();
}

}// namespace checks
}// namespace map_tests
}// namespace itl
#endif // ITL_MAP_CHECKS_H
