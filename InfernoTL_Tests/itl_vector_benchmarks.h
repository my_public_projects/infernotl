#ifndef ITL_VECTOR_BENCHMARKS_H
#define ITL_VECTOR_BENCHMARKS_H
#include <itl_vector.h>
#include <vector>
#include <string>
#include <chrono>
#include <iostream>
namespace itl
{
namespace vector_tests
{
/*!
 * \brief namespace for benchmarks
 */
namespace benchmarks
{

/*!
 * \brief vector_push_back_benchmark_iterations(long long, long long, int)
 * \param itl_time [out] result time of itl::vector
 * \param std_time [out] result time of std::vector
 * \param iterations_number [in] number of iterations for call push_back method.
 * \details Calls iterations_number  the  push_back method from std :: vector and itl :: vector and writes executing time to
 std_time and itl_time variables.
 */
void vector_push_back_benchmark_iterations(long long & itl_time, long long & std_time, int iterations_number)
{
    itl::vector<std::string> itl_vec;
    std::vector<std::string> std_vec;
    std::string s = "Hello, my darling!";

    //--------- itl test ---------
    std::chrono::system_clock::time_point itl_start_time;
    itl_start_time = std::chrono::system_clock::now(); // itl start time
    for(int i =0; i < iterations_number; ++i)
    {
        itl_vec.push_back(s);
    }

    std::chrono::system_clock::time_point itl_end_time;
    itl_end_time = std::chrono::system_clock::now(); // itl end time

    //--------- std test ---------
    std::chrono::system_clock::time_point std_start_time;
    std_start_time = std::chrono::system_clock::now(); // std start time

    for(int i = 0; i < iterations_number; ++i)
    {
        std_vec.push_back(s);
    }

    std::chrono::system_clock::time_point std_end_time;
    std_end_time = std::chrono::system_clock::now(); // std end time


    // Calculating delta time
    auto itl_delta = itl_end_time - itl_start_time;
    auto std_delta = std_end_time - std_start_time;

    itl_time = std::chrono::duration_cast<std::chrono::nanoseconds>(itl_delta).count();
    std_time = std::chrono::duration_cast<std::chrono::nanoseconds>(std_delta).count();

}

/*!
 * \brief vector_push_back_benchmark()
 * \details benchmarking of itl::vector::push_back(const T&) with cpu warm.
 */
void vector_push_back_benchmark()
{
    int repeates = 1000;
    int iterations_number = 10000;
    std::cout << "Starting itl::vector::push_back vs. "
                 "std::vector::push_back speed test\n"
              << repeates << " repeates\n";

    std::cout << iterations_number << " iterations\n";
    std::cout << "vector<std::string>\n";
    std::cout << "dataset: Hello, my darling!";
    std::vector<std::pair<long long, long long>> total;
    for(int i = 0; i < repeates; ++i)
    {
        long long itl_time;
        long long std_time;
        vector_push_back_benchmark_iterations(itl_time, std_time, iterations_number);
        total.push_back(std::pair<long long, long long>(itl_time, std_time));
    }

    std::cout << "\n******TOTAL******\n"
              << "itl::vector<int> vs std::vector<int>\n";

    for(auto & p : total)
    {
        std::string s;
        s += std::to_string(p.first) + " vs " +
            std::to_string(p.second) +
            " lose:" +
            std::to_string((p.first - p.second)) + "\n";
        std::cout << s;
    }

}


void vector_insert_benchmark_iterations(long long & itl_time, long long & std_time, int iterations_number)
{
    itl::vector<std::string> itl_vec;
    std::vector<std::string> std_vec;
    std::string s = "Hello, my darling!";

    //--------- itl test ---------
    std::chrono::system_clock::time_point itl_start_time;
    itl_start_time = std::chrono::system_clock::now(); // itl start time
    for(int i =0; i < iterations_number; ++i)
    {
        itl_vec.insert(0, s);
    }

    std::chrono::system_clock::time_point itl_end_time;
    itl_end_time = std::chrono::system_clock::now(); // itl end time

    //--------- std test ---------
    std::chrono::system_clock::time_point std_start_time;
    std_start_time = std::chrono::system_clock::now(); // std start time

    for(int i = 0; i < iterations_number; ++i)
    {
        std_vec.insert(std_vec.begin(), s);
    }

    std::chrono::system_clock::time_point std_end_time;
    std_end_time = std::chrono::system_clock::now(); // std end time


    // Calculating delta time
    auto itl_delta = itl_end_time - itl_start_time;
    auto std_delta = std_end_time - std_start_time;

    itl_time = std::chrono::duration_cast<std::chrono::nanoseconds>(itl_delta).count();
    std_time = std::chrono::duration_cast<std::chrono::nanoseconds>(std_delta).count();
}


/*!
 * \brief vector_isert_benchmark()
 * \details benchmarking of itl::vector::inser(const T&) with cpu warm.
 */
void vector_isert_benchmark()
{
    int repeates = 1000;
    int iterations_number = 1000;
    std::cout << "Starting itl::vector::insert(const T&) vs. "
                 "std::vector::insert( const_iterator, const T&) speed test\n"
              << repeates << " repeates\n";

    std::cout << iterations_number << " iterations\n";
    std::cout << "vector<std::string>\n";
    std::vector<std::pair<long long, long long>> total;
    for(int i = 0; i < repeates; ++i)
    {
        std::cout << "iteration" << i;
        long long itl_time;
        long long std_time;
        vector_insert_benchmark_iterations(itl_time, std_time, iterations_number);
        total.push_back(std::pair<long long, long long>(itl_time, std_time));
        std::cout << " result: itl " << itl_time << " vs std " << std_time <<
            " lose: " << itl_time - std_time <<"\n";
    }

    std::cout << "\n******TOTAL******\n"
              << "itl::vector<std::string> vs std::vector<std::string>\n";

    for(auto & p : total)
    {
        std::string s;
        s += std::to_string(p.first) + " vs " +
            std::to_string(p.second) +
            " lose:" +
            std::to_string((p.first - p.second)) + "\n";
        std::cout << s;
    }

}


} //namespace benchmarks




}// namespace vector_test
}// namespace itl
#endif // ITL_VECTOR_BENCHMARKS_H
