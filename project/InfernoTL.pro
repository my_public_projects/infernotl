#-------------------------------------------------
#
# Project created by QtCreator 2019-01-22T01:29:10
#
#-------------------------------------------------

QT       -= core gui

TARGET = InfernoTL
TEMPLATE = lib

INCLUDEPATH += $$PWD/../include


DEFINES += QT_DEPRECATED_WARNINGS

unix {
    target.path = /usr/lib
    INSTALLS += target
}

HEADERS += \
    ../include/itl_defines.h \
    ../include/itl_list.h \
    ../include/itl_map.h \
    ../include/itl_red_black_tree.h \
    ../include/itl_vector.h \
    ../include/itl_debug_dummy.h
