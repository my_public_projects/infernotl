#ifndef ITL_LIST_H
#define ITL_LIST_H

#include <assert.h>
#include "itl_defines.h"

namespace itl {
template<typename T>
/*!
 * \brief The teamplate class list
 * \author InfernalBoy
 * \details This class is analog of std::list. Why? For more experience!
 * T must meet the requirements of CopyConstructible, CopyAssignable, MoveAssignable, MoveConstructible.
 */
class list
{
    class list_item
    {
        friend class list;
        list_item * m_prev = nullptr;
        list_item * m_next = nullptr;
        T * m_data = nullptr;
    public:
        list_item()
        {
        }

        list_item(list_item * prev, list_item * next)
            : m_prev(prev),
              m_next(next)
        {
        }


        list_item(list_item * prev, list_item * next, const T & data)
            : m_prev(prev),
              m_next(next),
              m_data(new T(data))
        {
        }

        list_item(list_item * prev, list_item * next, T && data)
            : m_prev(prev),
              m_next(next),
              m_data(new T(std::move(data)))
        {
        }
        list_item (const list_item & r)
            : m_prev(r.m_prev),
              m_next(r.m_next),
              m_data(r.m_data? new T(*r.m_data):nullptr)
        {

        }
        list_item (list_item && r)
            : m_prev(r.m_prev),
              m_next(r.m_next),
              m_data(r.m_data)
        {
            r.m_data = nullptr;
        }
        list_item & operator=(const list_item & r)
        {
            if(m_data) delete m_data;
            m_data = r.m_data? new T(*r.m_data) : nullptr;
            m_prev = r.m_prev;
            m_next = r.m_next;
            return *this;
        }

        list_item & operator=( list_item && r)
        {
            T * t = m_data;
            m_data = r.m_data;
            r.m_data = t;
            m_prev = r.m_prev;
            m_next = r.m_next;
            return *this;
        }


        ~list_item()
        {
            delete m_data;
        }


        inline list_item * next() const
        {
            return m_next;
        }

        inline list_item * prev() const
        {
            return m_prev;
        }

        inline const T & data() const
        {
            return *m_data;
        }

        inline T & data()
        {
            return *m_data;
        }


    };

    void destroy()
    {
        while(m_size)
        {
            pop_front();
        }
    }
    void attach(const list & r)
    {
        for(auto & it: r)
        {
            push_back(it);
        }
    }

public:
    /*!
     * \brief The list_iterrator class
     * \details This class giving access to data from list. The instance of this class is not owning data
     */
    class list_iterrator
    {
        friend class list;
        list_item * m_item = nullptr;
    public:


        /*!
         * \brief list_iterrator() - simple constructor with null data
         *
         */
        list_iterrator()
        {

        }


        /*!
         * \brief list_iterrator(list_item*) - Constructor with link to data
         * \param item [in]
         */
        list_iterrator(list_item * item)
            :m_item(item)
        {
        }


        ~list_iterrator()
        {
            m_item = nullptr;
        }


        /*!
         * \brief operator++() - Sets Next element of list
         * \return reference to self
         */
        inline list_iterrator & operator++()
        {
            m_item = m_item->next();
            return *this;
        }


        /*!
         * \brief operator--() - Sets previous element of list
         * \return erence to self
         */
        inline list_iterrator & operator--()
        {
            m_item = m_item->prev();
            return *this;
        }


        /*!
         * \brief operator+(ull) - increments <num> times
         * \param num [in]
         * \return new list_iterator whith link to this + num element data
         */
        inline list_iterrator operator+(ull num) const
        {
            list_item * cur = m_item;
            while(num)
            {
                cur = cur->next();
                --num;
            }
            return list_iterrator(cur);
        }

        /*!
         * \brief operator-(ull) - decrements <num> times
         * \param num [in]
         * \return new list_iterator whith link to this - num element data
         */
        inline list_iterrator operator-(ull num) const
        {
            list_item * cur = m_item;
            while(num)
            {
                cur = cur->prev();
                --num;
            }
            return list_iterrator(cur);
        }



        list_iterrator operator+(int) const = delete ;
        list_iterrator operator-(int) const = delete ;


        /*!
         * \brief operator*()
         * \return reference to data of list element
         */
        inline T & operator*()
        {
            return m_item->data();
        }


        /*!
         * \brief operator*() const
         * \return reference to const data of list element
         */
        inline const T & operator*() const
        {
            return m_item->data();
        }


        /*!
         * \brief operator->()
         * \return pointer to data of list element
         */
        inline T * operator->()
        {
            return &m_item->data();
        }


        /*!
         * \brief operator->() const
         * \return pointer to const data of list element
         */
        inline const T * operator->() const
        {
            return &m_item->data();
        }


        /*!
         * \brief operator==() const
         * \param r [in]
         * \return true if this and r iterators sharing one instance of list_item
         */
        inline bool operator==(const list_iterrator & r) const noexcept
        {
            return  m_item == r.m_item;
        }

        /*!
         * \brief operator!=()
         * \param r [in]
         * \return true if this and r iterators do not sharing one instance of list_item
         */
        inline bool operator!=(const list_iterrator & r) const noexcept
        {
            return m_item != r.m_item;
        }


        /*!
         * \brief operator==()
         * \param r [in]
         * \return true if this iterator not referencing to nullptr and r is true
         */
        inline bool operator ==(bool r) const noexcept
        {
            return m_item == r;
        }


        /*!
         * \brief operator bool()
         * \return true if this iterator not referencing to nullptr
         */
        inline operator bool() const noexcept
        {
            return m_item;
        }

    };// class list_iterator


    /*!
     * \brief list()
     * \details Constructs an empty instance of list
     */
    list()
        : m_begin(new list_item()),
          m_end(m_begin),
          m_begin_it(m_begin),
          m_end_it(m_begin)
    {
    }


    /*!
     * \brief list(const list &)
     * \param r [in]
     * \details Copy Constructor from another list
     */
    list(const list & r)
        : m_begin(new list_item()),
          m_end(m_begin),
          m_begin_it(m_begin),
          m_end_it(m_begin)
    {
        attach(r);
    }


    /*!
     * \brief list(list &&)
     * \param r [in]
     * \details MoveConstuctor from another list
     */
    list(list && r)
    {
        m_begin = r.m_begin;
        m_end = r.m_end;
        m_begin_it = list_iterrator(m_begin);
        m_end_it = list_iterrator(m_end);

        m_size = r.m_size;
        r.m_end = nullptr;
        r.m_begin = nullptr;
        r.m_begin_it = list_iterrator();
        r.m_end_it = list_iterrator();
        r.m_size = 0;
    }


    /*!
     * \brief desturctor
     */
    ~list()
    {
        destroy();
        delete m_begin;
    }


    /*!
     * \brief operator=(const list &)
     * \param r [in]
     * \return reference to self
     * \details Assignment operator
     */
    list & operator=(const list & r)
    {
        destroy();
        attach(r);
        return  *this;
    }


    /*!
     * \brief operator=(list &&)
     * \param r [in]
     * \return reference to self
     * \details Move assignment operator
     */
    list & operator=(list && r)
    {
        list_iterrator * b_temp = m_begin;
        list_iterrator * e_temp = m_end;
        m_begin = r.m_begin;
        m_end = r.m_end;
        m_begin_it = list_iterrator(m_begin);
        m_end_it = list_iterrator(m_end);

        ull s_temp = m_size;
        m_size = r.m_size;
        r.m_begin = b_temp;
        r.m_end = b_temp;
        r.m_size = s_temp;
        return *this;
    }


    /*!
     * \brief push_back(const T &)
     * \param value [in]
     * \details Appends the given element value to the end of the list
     */
    void push_back(const T & value)
    {
        list_item * new_end = new list_item(m_end, nullptr);
        m_end->~list_item();
        new (m_end) list_item(m_end->m_prev?
                                  m_end->m_prev : nullptr,
                              new_end, value);
        m_end = new_end;
        m_end_it = list_iterrator(m_end);
        ++m_size;
    }


    /*!
     * \brief push_back(T &&)
     * \param value [in]
     * \details Appends the given element value to the end of the list
     */
    void push_back(T && value)
    {
        list_item * new_end = new list_item(m_end, nullptr);
        m_end->~list_item();
        new (m_end)list_item(m_end->m_prev?
                                 m_end->m_prev : nullptr,
                             new_end, std::move(value));
        m_end = new_end;
        m_end_it = list_iterrator(m_end);
        ++m_size;
    }


    /*!
     * \brief push_front(const T &)
     * \param value [in]
     * \details Appends the given element value to the begin of the list
     */
    void push_front(const T & value)
    {
        list_item * new_begin = new list_item(nullptr, m_begin, value);
        m_begin->m_prev = new_begin;
        m_begin = new_begin;
        m_begin_it = list_iterrator(m_begin);
        ++m_size;
    }


    /*!
     * \brief push_front(T &&)
     * \param value [in]
     * \details Appends the given element value to the begin of the list
     */
    void push_front(T && value)
    {
        list_item * new_begin = new list_item(nullptr, m_begin, std::move(value));
        m_begin->m_prev = new_begin;
        m_begin = new_begin;
        m_begin_it = list_iterrator(m_begin);
        ++m_size;
    }

    /*!
     * \brief pop_back()
     * \details Removes the last element of the list
     */
    void pop_back()
    {
        list_item * temp = m_end->m_prev;
        (temp->m_prev? temp->m_prev->m_next : m_end) = m_end;
        m_end->m_prev = temp->m_prev;
        delete temp;
        --m_size;
    }

    /*!
     * \brief pop_front()
     * \details Removes the first element of the list
     */
    void pop_front()
    {
        list_item * temp = m_begin->m_next;
        assert(temp);
        delete m_begin;
        m_begin = temp;
        m_begin->m_prev = nullptr;
        m_begin_it = list_iterrator(m_begin);
        --m_size;
    }

    /*!
     * \brief insert(list_iterrator, const T &)
     * \param pos [in]
     * \param value [in]
     * \return list_iterator referenсing to the new inserted element of the list
     * \details Inserts new element before pos
     */
    list_iterrator insert(list_iterrator pos, const T & value )
    {
        list_item * cur = pos.m_item;
        list_item * _new = new list_item(cur->m_prev, cur, value);
        if(cur->m_prev)
        {
            cur->m_prev->m_next = _new;
        }
        else
        {
            m_begin = _new;
            m_begin_it = list_iterrator(m_begin);
        }
        cur->m_prev = _new;
        ++m_size;
        return list_iterrator(_new);
    }

    /*!
     * \brief erase(list_iterator)
     * \param pos [in]
     * \return list_iterator to the next element after erased element
     * \details erase element from pos
     */
    list_iterrator erase(list_iterrator pos)
    {
        list_iterrator next = end();
        if(!m_size || pos == next)
        {
            return next;
        }
        next = pos + 1ull;

        list_item * cur = pos.m_item;
        if(cur->m_prev)
        {
            cur->m_prev->m_next = cur->m_next;
        }
        else
        {
            m_begin = cur->m_next;
            m_begin_it = list_iterrator(m_begin);
        }

        cur->m_next->m_prev = cur->m_prev;
        delete cur;
        --m_size;
        return next;
    }


    /*!
     * \brief clear()
     * \details Clear the container
     */
    void clear()
    {
        destroy();
    }


    /*!
     * \brief begin()
     * \return list_iterator to the begin of container
     */
    inline list_iterrator & begin() noexcept
    {
        return m_begin_it;
    }


    /*!
     * \brief end()
     * \return list_iterator to the next after last element of container
     */
    inline list_iterrator & end() noexcept
    {
        return m_end_it;
    }

    /*!
     * \brief begin() const
     * \return const list_iterator to the begin of container
     */
    inline const list_iterrator & begin() const noexcept
    {
        return m_begin_it;
    }


    /*!
     * \brief end() const
     * \return const list_iterator to the next after last element of container
     */
    inline const list_iterrator & end() const noexcept
    {
        return m_end_it;
    }


    /*!
     * \brief cbegin() const
     * \return const list_iterator to the begin of container
     */
    inline const list_iterrator & cbegin() noexcept
    {
        return m_begin_it;
    }

    /*!
     * \brief cend() const
     * \return const list_iterator to the end of container
     */
    inline const list_iterrator & cend() noexcept
    {
        return m_end_it;
    }

    /*!
     * \brief front()
     * \return reference to the first element of list
     */
    inline T & front()
    {
        return *m_begin_it;
    }



    /*!
     * \brief front() const
     * \return reference to the const first element of list
     */
    inline const T & front() const
    {
        return *m_begin_it;
    }


    /*!
     * \brief back()
     * \return reference to the last element
     */
    inline T & back()
    {
        return *(m_end_it - 1);
    }


    /*!
     * \brief back() const
     * \return reference to the const last element
     */
    inline const T & back() const
    {
        return *(m_end_it - 1);
    }


    /*!
     * \brief size() const
     * \return count of list elements
     */
    inline ull size() const noexcept
    {
        return m_size;
    }


    /*!
     * \brief empty() const
     * \return true if container is empty
     */
    inline bool empty() const noexcept
    {
        return begin() == end();
    }


    /*!
     * \brief operator==(const list &)
     * \param r [in]
     * \return true if all elements of both lists is equal
     */
    bool operator==(const list & r) const noexcept
    {
        if(r.m_size != m_size)
        {
            return false;
        }
        list_iterrator it = m_begin_it;
        for (const T & e:r) {
            if(*it != e)
            {
                return false;
            }
            ++it;
        }
        return true;
    }


    /*!
     * \brief operator!=(const list &)
     * \param r [in]
     * \return true if lists is not equal
     */
    bool operator!=(const list & r) const noexcept
    {
        if(r.m_size != m_size)
        {
            return true;
        }


        list_iterrator it = m_begin_it;
        for (const T & e:r) {
            if(*it != e)
            {
                return true;
            }
            ++it;
        }
        return false;

    }
private:
    list_item * m_begin = nullptr;
    list_item * m_end = nullptr;
    list_iterrator m_begin_it;
    list_iterrator m_end_it;
    ull m_size = 0;
};



}
#endif // ITL_LIST_H
