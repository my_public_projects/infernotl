#ifndef ITL_DEBUG_DUMMY_H
#define ITL_DEBUG_DUMMY_H
#include <iostream>
namespace itl {
class debug_dummy
{

public:
    debug_dummy()
    {
        std::cout << "debug_dummy::debug_dummy()\n";
    }

    debug_dummy(const debug_dummy &)
    {
        std::cout << "debug_dummy::debug_dummy(const debug_dummy&)\n";
    }

    debug_dummy(const debug_dummy &&)
    {
        std::cout << "debug_dummy::debug_dummy(const debug_dummy &&)\n";
    }

    debug_dummy & operator=(const debug_dummy &)
    {
        std::cout << "debug_dummy::operator=(const debug_dummy &)\n";
        return  *this;
    }

    debug_dummy & operator=(const debug_dummy &&)
    {
        std::cout << "debug_dummy::operator=(const debug_dummy &&)\n";
        return  *this;
    }

    ~debug_dummy()
    {
        std::cout << "debug_dummy::~debug_dummy()\n";
    }



};
}// namespace
#endif // ITL_DEBUG_DUMMY_H
