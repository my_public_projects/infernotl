TEMPLATE = app
CONFIG += console
CONFIG += c++1z
CONFIG -= app_bundle
CONFIG -= qt




INCLUDEPATH += ../include

SOURCES += \
        main.cpp

HEADERS += \
    itl_list_benchmarks.h \
    itl_list_checks.h \
    itl_map_benchmarks.h \
    itl_map_checks.h \
    itl_vector_benchmarks.h \
    itl_memcopy_benchmark.h \
    itl_vector_checks.h
