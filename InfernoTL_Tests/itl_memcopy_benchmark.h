#ifndef ITL_MEMCOPY_BENCHMARK_H
#define ITL_MEMCOPY_BENCHMARK_H
#include <itl_defines.h>
#include <iostream>
#include <chrono>
#include <cstring>
#include <vector>
namespace itl {
namespace memory_test {

void mem_copy_benchmark_iterations(long long & my_time, long long & c_time, const std::string & dataset, int iterations_number)
{

    itl::ull count = dataset.size();
    char * dest = new char[count];
    char * src = const_cast<char *>(dataset.c_str());
    char * clear = new char[count];

    std::chrono::system_clock::time_point my_start_time = std::chrono::system_clock::now();

    for(int i = 0; i < iterations_number; ++i)
    {
        itl::mem_copy(src, dest, count);
        itl::mem_copy(clear, dest, count);
    }
    std::chrono::system_clock::time_point my_end_time;
    my_end_time = std::chrono::system_clock::now();

    std::chrono::system_clock::time_point c_start_time;
    c_start_time = std::chrono::system_clock::now();
    for(int i = 0; i < iterations_number; ++i)
    {
        memcpy(dest, src, count);
        memcpy(dest, clear, count);
    }
    std::chrono::system_clock::time_point c_end_time;
    c_end_time = std::chrono::system_clock::now();

    auto my_delta = my_end_time - my_start_time;
    auto c_delta = c_end_time - c_start_time;

    my_time = std::chrono::duration_cast<std::chrono::nanoseconds>(my_delta).count();
    c_time = std::chrono::duration_cast<std::chrono::nanoseconds>(c_delta).count();
    delete [] dest;
    delete [] clear;
}

struct result
{
    long long m_my_time;
    long long m_c_time;
    std::string m_dataset="fuck";

    result(long long my_time, long long c_time, const std::string & dataset):
                                                                              m_my_time(my_time),
                                                                              m_c_time(c_time)
    {
        m_dataset = dataset;

    }

    result(const result& r)
    {
        m_my_time = r.m_my_time;
        m_c_time = r.m_c_time;
        m_dataset = r.m_dataset;
        //std::cout << "********r****";
    }

    result(const result&& r)
    {
        m_my_time = r.m_my_time;
        m_c_time = r.m_c_time;
        m_dataset = r.m_dataset;
        //std::cout << "********r****";
    }

    result & operator=(const result & r)
    {
        m_my_time = r.m_my_time;
        m_c_time = r.m_c_time;
        m_dataset = r.m_dataset;
        //std::cout << "********r****";
        return *this;
    }

    result & operator=(const result && r)
    {
        m_my_time = r.m_my_time;
        m_c_time = r.m_c_time;
        m_dataset = r.m_dataset;
        //std::cout << "********r****";
        return *this;
    }
    ~result(){}
};

void mem_copy_benchmark()
{
    int repeates = 1000;
    int iterations_number = 100000;
    std::string dataset = "Hello, my darling! Hello hello, my darling! Hello, my darling!";
    std::cout << "Starting speed test\n" << repeates << " repeates\n";
    std::cout << iterations_number << " iterations\n";
    std::cout << "Dataset: " << dataset << "\n";
    std::vector<result> total;
    for(int i = 0; i < repeates; ++i)
    {
        long long my_time;
        long long c_time;
        // std::cout << temp;
        mem_copy_benchmark_iterations(my_time, c_time, dataset, iterations_number);
        total.push_back(result(my_time, c_time, dataset));
        //std::cout << total.back().m_dataset << "\n";
    }

    std::cout << "\n******TOTAL******\n" << "itl::mem_copy vs memcpy\n";
    for(auto & r : total)
    {
        std::string s;
        s += std::to_string(r.m_my_time) + " vs " +
            std::to_string(r.m_c_time) +
            " lose:" +
            std::to_string((r.m_my_time - r.m_c_time)) +  " dataset:" + r.m_dataset + "\n";
        std::cout << s;
    }

}


}// namespace memory_test
}// namespace itl
#endif // ITL_MEMCOPY_BENCHMARK_H
