#ifndef ITL_DEFINES_H
#define ITL_DEFINES_H
#include <cstring>
#include <iostream>
namespace itl {

using ull = unsigned long long int;

template<typename T1, typename T2>
/*!
 * \brief The pair<T1, T2> template struct
 * \details Holds pair data T1 and T2
 * T1 and T2 must meet the requirements of CopyAssignable, MoveAssignable, MoveConstructible.
 */
struct pair
{
    T1 m_first;
    T2 m_second;

    pair()
    {
    }


    pair(const T1 & first, const T2 & second)
        : m_first(first),m_second(second)
    {
    }

    pair(T1 && first, T2 && second)
        : m_first(std::move(first)),m_second(std::move(second))
    {
    }

    pair(const pair &) = default;
    pair(pair &&) = default;
    pair & operator=(const pair &) = default;
    pair & operator=( pair &&) = default;



};

/*! 
 * \brief Copy bytes from src to dest
 * @param[in] src The buffer to copy from.
 * @param[out] dest The buffer to copy to.
 * @param[in] count The number of bytes to copy.
 */
void mem_copy(char * src, char * dest, ull count)
{
    char * end = src + count;
    ull it_number = count / 8;
    ull * srcl = reinterpret_cast<ull*>(src);
    ull * destl = reinterpret_cast<ull*>(dest);

    // Copying the buffer by blocks with size 8 bytes
    for(ull i = 0; i < it_number; ++i)
    {
        *(destl++) = *(srcl++);
    }

    // Copying the tail's bytes
    src = reinterpret_cast<char *>(srcl);
    dest = reinterpret_cast<char *>(destl);
    while (src != end)
    {
        *(dest++) = *(src++);
    }


}

}// namesapce itl
#endif // ITL_DEFINES_H
